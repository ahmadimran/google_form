const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const SCOPES = ['https://www.googleapis.com/auth/script.projects','https://www.googleapis.com/auth/forms'];
const TOKEN_PATH = 'token.json';

fs.readFile('./test.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    console.log(JSON.parse(content))
  // Authorize a client with credentials, then call the Google Apps Script API.
  authorize(JSON.parse(content), callAppsScript);
});

function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.web;
    console.log(redirect_uris[0])
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

function getAccessToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    approval_prompt:'force',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

// /**
//  * Creates a new script project, upload a file, and log the script's URL.
//  * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
//  */
function callAppsScript(auth) {
  const script = google.script({ version: 'v1', auth });
//   let form_id='1NMwZtMSsNNjCwnpDx40OyXaAa7hIrZ-5q4D8juzyY7Q'
  // script.projects.create({
  //   resource: {
  //     title: 'Email_Getter',
  //   },
//   }, async(err, res)  => {
//     if (err) return console.log(`The API create method returned an error: ${err}`);
//     script.projects.updateContent({
//       scriptId: res.data.scriptId,
//       auth,
//       resource: {
//         files: [{
//           name: 'email',
//           type: 'SERVER_JS',
//           source: `function myFunction() {
//             var form = FormApp.openById("1NMwZtMSsNNjCwnpDx40OyXaAa7hIrZ-5q4D8juzyY7Q");
//             var item = form.addCheckboxItem();
//            var form_id=form.getId();
//                 var Published= form.getPublishedUrl()
//             // Logger.log('Published URL: ' + form.getPublishedUrl());
//            var res= getresponse(form_id)
//            var email=GetRespondersEmail(form)
//            Logger.log(res)
//             Logger.log('Editor URL: ' + getresponse(form_id));
//             var data={};
//             data.url=form.getPublishedUrl();
//             data.form_id=form_id
//             data.form_response=res
//             data.email=email
//             return data
//             }


// function getresponse(form_id){
// var form = FormApp.openById(form_id);
// var formResponses = form.getResponses();
// var arr=[]
//   for (var i = 0; i < formResponses.length; i++) {
//    var formResponse = formResponses[i];
//    var itemResponses = formResponse.getItemResponses();
//    for (var j = 0; j < itemResponses.length; j++) {
//     var itemResponse = itemResponses[j];
//     var data={}
//       data.number =(i + 1).toString();
//        data.response_title= itemResponse.getItem().getTitle();
//         data.response_item= itemResponse.getResponse();
//         arr.push(data)
//  }
//  }
//  return arr
// }
// function GetRespondersEmail(form){
//   var emailTo = []
//   var responses=form.getResponses();
// /// this will give you all responses of your form as an array////

// ///iterate the array to get respondent email id///

// for(var i = 0; i < responses.length; i++){
//    emailTo[i] = responses[i].getRespondentEmail();     
// }
//   Logger.log('emailTo = '+emailTo);
//   return emailTo;
// };`,
//         }, {
//           name: 'appsscript',
//           type: 'JSON',
//           source: '{\"timeZone\":\"America/New_York\",\"exceptionLogging\":' +
//            '\"CLOUD\"}',
//         }],
//       },
//     }, {}, (err, res) => {
//       if (err) return console.log(`The API updateContent method returned an error: ${err}`);
//       console.log(`https://script.google.com/d/${res.data.scriptId}/edit`);
//     });
    script.scripts.run({
      "scriptId":"1wW7XCuOXpaZzFxT0Cs7ZN4o1nkNL2n1FJnN4kB6Smm_OQk3A1329zpmu",
      "auth": auth,
      "resource": {
        "function": 'myFunction',
      },
      "devMode": true
    }, function(err, resp) {
      if (err) {
        console.log("Error started --------------------------------------")
        console.log(err)
        console.log("Error ended --------------------------------------")
        // The API encountered a problem before the script started executing.
        console.log('The API returned an error: ' + err);
        return;
      }
      if (resp.error) {
        const error = resp.error.details[0];
        console.log('Script error message: ' + error.errorMessage);
        console.log('Script error stacktrace:');
    
        if (error.scriptStackTraceElements) {
          // There may not be a stacktrace if the script didn't start executing.
          for (let i = 0; i < error.scriptStackTraceElements.length; i++) {
            const trace = error.scriptStackTraceElements[i];
            console.log('\t%s: %s', trace.function, trace.lineNumber);
          }
        }
      } else {
        // console.log(resp)
        if (resp.data) {
          if (resp.data.response) {
            console.log('----------Result----------')
            console.log(resp.data.response.result)
            console.log('----------End Result----------')
          }
        }

      }
    });
  // });
}